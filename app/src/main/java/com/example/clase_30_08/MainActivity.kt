package com.example.clase_30_08

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val btnHello : Button = findViewById(R.id.button)

        btnHello.setOnClickListener{ lookvalue() }
    }

    fun lookvalue(){
        val etName: EditText = findViewById(R.id.editTextTextPersonName)
        if(etName.text.isEmpty()){
            showError()
        }else{
            val intent = Intent (this, MainActivity2::class.java)
            intent.putExtra("Username", etName.text)
            startActivity(intent)

        }
    }

    fun showError(){
        Toast.makeText(this, "Type a name", Toast.LENGTH_LONG).show()
    }
}