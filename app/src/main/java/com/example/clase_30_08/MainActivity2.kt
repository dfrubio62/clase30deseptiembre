package com.example.clase_30_08

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        showInfo()
        val btnBack = findViewById<Button>(R.id.btnback)
        btnBack.setOnClickListener{ onBackPressed() }
    }

    fun showInfo(){
        val bundle = intent.extras
        val name = bundle?.get("USER_NAME").toString()

        val tv:TextView = findViewById(R.id.textView)
        tv.setText(name)
    }
}